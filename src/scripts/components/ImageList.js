export class ImageList {
    constructor() {
        this.list = [];
        this.selectors();
        this.events();
    }
    selectors() {
        this.form = document.querySelector(".add-form");

        this.imageInput = document.querySelector(".add-image-form-input");

        this.titleInput = document.querySelector(".add-title-form-input");

        this.textDescription = document.querySelector(".add-description-form-input");

        this.items = document.querySelector(".container-images-descriptions");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
        this.imageInput.addEventListener("change", this.readImage);
    }

    addItemToList(event) {
        event.preventDefault();
        const imageInput = this.imageInput.files[0];
        const itemTitle = this.titleInput.value;
        const itemArea = this.textDescription.value;

        if (imageInput && itemTitle && itemArea) {
            let file = new FileReader();
            file.onload = function (e) {
                document.getElementById("preview").src = e.target.result;
                const item = {
                    file: e.target.result,
                    title: itemTitle,
                    desc: itemArea,
                };
                this.list.push(item);
                this.renderListItems();
                this.resetInputs();
            }.bind(this);

            file.readAsDataURL(imageInput);
        }
    }
    readImage() {
        if (this.files && this.files[0]) {
            var file = new FileReader();
            file.onload = function (e) {
                document.querySelector(".preview").src = e.target.result;
            };
            file.readAsDataURL(this.files[0]);
        }
    }

    renderListItems(list) {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure = `
            <div class="images-container container">
                <img class="container-image" src="${item.file}"/>
                    <div class="description-container">
                        <h1 class="title-container">${item.title}</h1>
                        <p class="subtitle-container">${item.desc}</p>
                    </div>
            </div>`;
        });

        this.items.innerHTML += itemsStructure;
    }
    resetInputs() {
        document.querySelector(".preview").src = "";
        this.imageInput.value = "";
        this.titleInput.value = "";
        this.textDescription.value = "";
    }
}
